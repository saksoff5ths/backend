package com.saksoffth.backend.entity;

public enum PurchaseOrderStatus {
    NEW, ACCEPTED, READY_FOR_DELIVERY, IN_DELIVERY, DELIVERED, CLOSED
}

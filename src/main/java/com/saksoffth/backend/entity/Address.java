package com.saksoffth.backend.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

/**
 * Domain model for dat_address table.
 */
@Getter
@Setter
@Entity
@Table(name = "dat_address")
@SQLDelete(sql = "UPDATE dat_address SET deleted = true WHERE id=?")
@Where(clause = "(deleted is null or deleted = false)")
public class Address extends BaseEntity {

    @Column(nullable = false)
    private String name;
    @Column(nullable = false, name = "zip_code")
    private String zipCode;
    @Column(length = 20)
    private String phone;
    @Column(nullable = false)
    private String line1;
    @Column
    private String line2;
    @Column(nullable = false)
    private String state;
    @Column(nullable = false)
    private String city;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id")
    private User userId;

}

package com.saksoffth.backend.entity;

import lombok.Getter;

@Getter
public enum UserRole {
    ADMIN,
    CUSTOMER
}

package com.saksoffth.backend.entity;

import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

/**
 * Domain model for dat_product table.
 */
@Setter
@Getter
@Entity
@Table(name = "dat_product")
@SQLDelete(sql = "UPDATE dat_product SET deleted = true WHERE id=?")
@Where(clause = "(deleted is null or deleted = false)")
public class Product extends BaseEntity {

    @Column(nullable = false)
    private String sku;
    @Column(nullable = false)
    private String name;
    @Column
    private String description;
    @Column(nullable = false)
    private BigDecimal price;
    @Column(nullable = false)
    private BigDecimal quantity;
    @Column
    private boolean active;
    @Column
    private String meta;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "category_id")
    private Category category;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "vendor_id")
    private Vendor vendor;

    // TODO: 5/19/2022 add image
}

package com.saksoffth.backend.entity;

import java.math.BigDecimal;
import java.time.Instant;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;
import org.springframework.data.annotation.CreatedBy;

/**
 * Domain model for dat_purchase_order table.
 */
@Getter
@Setter
@Entity
@Table(name = "dat_purchase_order")
@SQLDelete(sql = "UPDATE dat_purchase_order SET deleted = true WHERE id=?")
@Where(clause = "(deleted is null or deleted = false)")
public class PurchaseOrder extends BaseEntity {
    @Column
    private String note;
    @Column(name = "delivery_price", nullable = false)
    private BigDecimal deliveryPrice;
    @Column(name = "discount")
    private BigDecimal discount;
    @Column(name = "sub_total")
    private BigDecimal subTotal;
    @Column(nullable = false)
    private BigDecimal total;

    @CreationTimestamp
    @Column(name = "created_date")
    private Instant createDate;
    @Column(name = "delivery_date")
    private Instant deliveryDate;
    @CreatedBy
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id")
    private User user;

    @Column(name = "pay_status")
    @Enumerated(EnumType.STRING)
    private PurchaseOrderPayStatus payStatus;
    @Column
    @Enumerated(EnumType.STRING)
    private PurchaseOrderStatus status;
}

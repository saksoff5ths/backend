package com.saksoffth.backend.entity;

/**
 * {@link User} status enum.
 */
public enum UserStatus {
    ACTIVE, BLOCKED
}

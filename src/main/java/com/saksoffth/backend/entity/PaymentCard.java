package com.saksoffth.backend.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

/**
 * Domain model for dat_payment_card table.
 */
@Getter
@Setter
@Entity
@Table(name = "dat_payment_card")
@SQLDelete(sql = "UPDATE dat_payment_card SET deleted = true WHERE id=?")
@Where(clause = "(deleted is null or deleted = false)")
public class PaymentCard extends BaseEntity {

    @Column(nullable = false)
    private String holder;
    @Column(name = "masked_number")
    private String maskedNumber;
    @Column(name = "masked_valid_thru")
    private String maskedValidThru;
    @Column(name = "token")
    private String token;
    @Enumerated(EnumType.STRING)
    @Column(nullable = false)
    private PaymentCardStatus status;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id")
    private User userId;

}

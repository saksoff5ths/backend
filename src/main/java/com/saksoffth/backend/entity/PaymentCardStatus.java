package com.saksoffth.backend.entity;

public enum PaymentCardStatus {
    DRAFT, ACTIVE, BLOCKED
}

package com.saksoffth.backend.entity;

public enum PurchaseOrderPayStatus {
    UNPAID, PAID
}

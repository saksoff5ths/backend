package com.saksoffth.backend.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

/**
 * Domain model for dat_vendor table.
 */
@Getter
@Setter
@Entity
@Table(name = "dat_vendor")
@SQLDelete(sql = "UPDATE dat_vendor SET deleted = true WHERE id=?")
@Where(clause = "(deleted is null or deleted = false)")
public class Vendor extends BaseEntity {

    @Column(nullable = false)
    private String name;
    @Column(nullable = false)
    private String code;
    @Column(nullable = false)
    private Double deliveryDays;
}

package com.saksoffth.backend.entity;

import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

/**
 * Domain model for dat_purchase_order_item table.
 */
@Getter
@Setter
@Entity
@Table(name = "dat_purchase_order_item")
@SQLDelete(sql = "UPDATE dat_purchase_order_item SET deleted = true WHERE id=?")
@Where(clause = "(deleted is null or deleted = false)")
public class PurchaseOrderItem extends BaseEntity {

    @Column(name = "quantity", nullable = false)
    private BigDecimal quantity;
    @Column(name = "unit_price", nullable = false)
    private BigDecimal unitPrice;
    @Column(name = "total_unit_price", nullable = false)
    private BigDecimal totalUnitPrice;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "purchase_order_id")
    private PurchaseOrder purchaseOrder;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "product_id")
    private Product product;
}

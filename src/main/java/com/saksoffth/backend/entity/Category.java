package com.saksoffth.backend.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

/**
 * Domain model for dat_category table.
 */
@Getter
@Setter
@Entity
@Table(name = "dat_category")
@SQLDelete(sql = "UPDATE dat_category SET deleted = true WHERE id=?")
@Where(clause = "(deleted is null or deleted = false)")
public class Category extends BaseEntity {

    @Column(nullable = false)
    private String name;
    @Column(length = 500)
    private String description;
    @Column(length = 100, nullable = false)
    private String code;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "parent_id")
    private Category parent;
}

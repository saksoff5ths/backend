package com.saksoffth.backend.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

/**
 * Domain model for dat_user table.
 */
@Getter
@Setter
@Entity
@Table(name = "dat_user")
@SQLDelete(sql = "UPDATE dat_user SET deleted = true WHERE id=?")
@Where(clause = "(deleted is null or deleted = false)")
public class User extends BaseEntity {

    @Column(nullable = false)
    private String email;
    @Column
    private String password;
    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private UserRole role;
    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private UserStatus status;
}

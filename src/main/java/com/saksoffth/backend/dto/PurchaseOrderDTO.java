package com.saksoffth.backend.dto;

import io.swagger.v3.oas.annotations.media.Schema;

/**
 * DTO class for {@link com.saksoffth.backend.entity.PurchaseOrder} entity.
 */
@Schema(description = "Purchase order model")
public record PurchaseOrderDTO(@Schema(description = "Id") Long id) {
}

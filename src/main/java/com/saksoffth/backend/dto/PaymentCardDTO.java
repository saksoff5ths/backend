package com.saksoffth.backend.dto;

import com.saksoffth.backend.entity.PaymentCard;
import io.swagger.v3.oas.annotations.media.Schema;

/**
 * DTO class for {@link PaymentCard} entity.
 */
@Schema(description = "Payment card model")
public record PaymentCardDTO(@Schema(description = "Payment card id.") Long id) {
}

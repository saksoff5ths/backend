package com.saksoffth.backend.dto;

import io.swagger.v3.oas.annotations.media.Schema;

/**
 * DTO class for {@link com.saksoffth.backend.entity.Product} entity.
 */
@Schema(description = "Product model")
public record ProductDTO(@Schema(description = "Product id") Long id,
                         @Schema(description = "SKU") String sku,
                         @Schema(description = "Product name") String name,
                         @Schema(description = "Product description") String description,
                         @Schema(description = "Product category") CategoryDTO category) {
}

package com.saksoffth.backend.dto;

import com.saksoffth.backend.entity.Address;
import io.swagger.v3.oas.annotations.media.Schema;

/**
 * DTO class for {@link Address} entity.
 */
@Schema(description = "Category model")
public record AddressDTO(@Schema(description = "Address Id") Long id) {
}

package com.saksoffth.backend.dto;

import javax.validation.constraints.NotEmpty;

import com.saksoffth.backend.entity.Category;
import io.swagger.v3.oas.annotations.media.Schema;

/**
 * DTO class for {@link Category} entity.
 */
@Schema(description = "Category model")
public record CategoryDTO(@Schema(description = "Category Id") Long id,
                          @NotEmpty @Schema(description = "Category name") String name,
                          @Schema(description = "Category description") String description,
                          @NotEmpty @Schema(description = "Category code") String code,
                          @Schema(description = "Category parent") SelectableDTO parent) {
}

package com.saksoffth.backend.dto;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import com.saksoffth.backend.entity.User;
import com.saksoffth.backend.entity.UserRole;
import io.swagger.v3.oas.annotations.media.Schema;

/**
 * DTO for {@link User} entity.
 */
@Schema(description = "User model")
public record UserDTO(@Schema(description = "Id") Long id,
                      @NotEmpty @Schema(description = "email") String email,
                      @NotNull @Schema(description = "Role. One of: ADMIN, CUSTOMER") UserRole role) {
}

package com.saksoffth.backend.dto;

import javax.validation.constraints.NotEmpty;

import com.saksoffth.backend.entity.User;
import io.swagger.v3.oas.annotations.media.Schema;

/**
 * DTO for {@link User} entity.
 */
@Schema(description = "Password model")
public record PasswordDTO(@NotEmpty @Schema(description = "Password") String password) {
}

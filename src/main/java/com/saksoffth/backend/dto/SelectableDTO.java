package com.saksoffth.backend.dto;

import io.swagger.v3.oas.annotations.media.Schema;

/**
 * Selectable model.
 */
@Schema(description = "Selectable model")
public record SelectableDTO(@Schema(description = "Id") Long id, @Schema(description = "Name") String name) {
}

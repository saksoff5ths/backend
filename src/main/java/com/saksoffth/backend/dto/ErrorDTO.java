package com.saksoffth.backend.dto;

import java.util.Map;

import io.swagger.v3.oas.annotations.media.Schema;

/**
 * DTO class for error response.
 */
@Schema(description = "Error model")
public record ErrorDTO(@Schema(description = "Error status code") int status,
                       @Schema(description = "Error message") String message,
                       @Schema(description = "Validated field error messages") Map<String, Object> meta) {
}

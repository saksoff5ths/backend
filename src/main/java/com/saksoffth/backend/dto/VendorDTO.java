package com.saksoffth.backend.dto;

import javax.validation.constraints.NotNull;

import io.swagger.v3.oas.annotations.media.Schema;

/**
 * DTO class for {@link com.saksoffth.backend.entity.Vendor} entity.
 */
@Schema(description = "Vendor model")
public record VendorDTO(@Schema(description = "Id") Long id,
                        @NotNull @Schema(description = "Vendor name") String name,
                        @NotNull @Schema(description = "Code") String code,
                        @NotNull @Schema(description = "Delivery days") Double deliveryDays) {
}

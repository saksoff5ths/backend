package com.saksoffth.backend.dto;

import io.swagger.v3.oas.annotations.media.Schema;

/**
 * DTO class for {@link com.saksoffth.backend.entity.PurchaseOrderItem} entity.
 */
@Schema(description = "Purchase order item model")
public record PurchaseOrderItemDTO(@Schema(description = "Id") Long id) {
}

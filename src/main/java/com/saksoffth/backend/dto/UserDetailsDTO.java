package com.saksoffth.backend.dto;

import java.util.Collection;

import com.saksoffth.backend.entity.UserRole;
import com.saksoffth.backend.entity.UserStatus;
import lombok.Getter;
import lombok.Setter;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

/**
 * Spring security model for {@link com.saksoffth.backend.entity.User}.
 */
@Setter
@Getter
public class UserDetailsDTO implements UserDetails {
    private Long id;
    private String email;
    private String password;
    private UserRole role;
    private UserStatus status;
    private Collection<SimpleGrantedAuthority> authorities;

    @Override
    public String getUsername() {
        return email;
    }

    @Override
    public boolean isAccountNonExpired() {
        return status == UserStatus.ACTIVE;
    }

    @Override
    public boolean isAccountNonLocked() {
        return status == UserStatus.ACTIVE;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return status == UserStatus.ACTIVE;
    }

    @Override
    public boolean isEnabled() {
        return status == UserStatus.ACTIVE;
    }
}

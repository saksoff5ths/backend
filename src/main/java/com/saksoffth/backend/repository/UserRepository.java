package com.saksoffth.backend.repository;

import java.util.Optional;

import com.saksoffth.backend.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Repository class for {@link User} class.
 */
public interface UserRepository extends JpaRepository<User, Long> {
    Optional<User> findByEmail(String email);

}

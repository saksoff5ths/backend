package com.saksoffth.backend.repository;

import com.saksoffth.backend.entity.PaymentCard;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Repository class for {@link PaymentCard} class.
 */
public interface PaymentCardRepository extends JpaRepository<PaymentCard, Long> {
}

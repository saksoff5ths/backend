package com.saksoffth.backend.repository;

import com.saksoffth.backend.entity.PurchaseOrderItem;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Repository class for {@link PurchaseOrderItem} class.
 */
public interface PurchaseOrderItemRepository extends JpaRepository<PurchaseOrderItem, Long> {
}

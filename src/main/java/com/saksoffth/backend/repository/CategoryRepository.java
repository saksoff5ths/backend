package com.saksoffth.backend.repository;

import java.util.List;

import com.saksoffth.backend.entity.Category;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

/**
 * Repository class for {@link Category} class.
 */
public interface CategoryRepository extends JpaRepository<Category, Long> {

    /**
     * Search for category by name.
     *
     * @param searchKey key for search by name.
     * @return instance of {@link List<Category>}.
     */
    @Query("select c from Category c where c.deleted = false and lower(c.name) like :searchKey")
    List<Category> findAllByNameLike(String searchKey);

    /**
     * Returns all parent categories.
     *
     * @return instance of {@link List<Category>}
     */
    List<Category> findAllByParentIsNull();

    /**
     * Returns category list by parent category.
     *
     * @param category instance of {@link Category}
     * @return instance of {@link List<Category>}
     */
    List<Category> findAllByParent(Category category);
}

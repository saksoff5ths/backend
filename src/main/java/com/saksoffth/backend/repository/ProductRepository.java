package com.saksoffth.backend.repository;

import java.util.List;

import com.saksoffth.backend.entity.Category;
import com.saksoffth.backend.entity.Product;
import com.saksoffth.backend.entity.Vendor;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

/**
 * Repository class for {@link Product} class.
 */
public interface ProductRepository extends JpaRepository<Product, Long> {

    void deleteAllByCategory(Category category);

    void deleteAllByVendor(Vendor vendor);

    @Query(value = "select p from Product p where p.active is null or p.active = true ")
    List<Product> findAllActive(Pageable pageable);

    @Query(value = "select p from Product p where (p.active is null or p.active = true) and p.category.id = :categoryId")
    List<Product> findAllActiveAndCategoryId(Long categoryId, Pageable pageable);
}

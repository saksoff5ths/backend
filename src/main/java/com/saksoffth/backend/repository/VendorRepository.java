package com.saksoffth.backend.repository;

import com.saksoffth.backend.entity.Vendor;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Repository class for {@link Vendor} class.
 */
public interface VendorRepository extends JpaRepository<Vendor, Long> {
}

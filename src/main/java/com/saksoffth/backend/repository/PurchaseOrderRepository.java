package com.saksoffth.backend.repository;

import com.saksoffth.backend.entity.PurchaseOrder;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Repository class for {@link PurchaseOrder} class.
 */
public interface PurchaseOrderRepository extends JpaRepository<PurchaseOrder, Long> {
}

package com.saksoffth.backend.repository;

import com.saksoffth.backend.entity.Address;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Repository class for {@link Address} class.
 */
public interface AddressRepository extends JpaRepository<Address, Long> {
}

package com.saksoffth.backend.controller;

import com.saksoffth.backend.api.AuthenticationApi;
import com.saksoffth.backend.config.security.JwtTokenProvider;
import com.saksoffth.backend.dto.LoginDTO;
import com.saksoffth.backend.dto.UserSessionDTO;
import com.saksoffth.backend.mapper.UserMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
public class UserLoginController implements AuthenticationApi {

    private final UserMapper userMapper;
    private final JwtTokenProvider tokenProvider;
    private final AuthenticationManager authenticationManager;

    @Override
    public ResponseEntity<UserSessionDTO> login(LoginDTO loginDTO) {
        var authenticationToken = userMapper.toAuthToken(loginDTO);
        var authentication = authenticationManager.authenticate(authenticationToken);
        var token = tokenProvider.generateToken(authentication);

        SecurityContextHolder.getContext().setAuthentication(authentication);
        return ResponseEntity.ok(userMapper.toUserSessionToken(token));
    }
}

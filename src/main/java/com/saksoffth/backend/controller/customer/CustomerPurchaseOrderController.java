package com.saksoffth.backend.controller.customer;

import java.util.List;

import com.saksoffth.backend.api.CustomerPurchaseOrderApi;
import com.saksoffth.backend.dto.CustomerPurchaseOrderDTO;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

/**
 * Main implementation of {@link CustomerPurchaseOrderApi}
 */
@RestController
@RequiredArgsConstructor
public class CustomerPurchaseOrderController implements CustomerPurchaseOrderApi {

    @Override
    public ResponseEntity<CustomerPurchaseOrderDTO> createPurchaseOrder(CustomerPurchaseOrderDTO body) {
        return null;
    }

    @Override
    public ResponseEntity<CustomerPurchaseOrderDTO> getItem(Long id) {
        return null;
    }

    @Override
    public ResponseEntity<List<CustomerPurchaseOrderDTO>> getList(Integer limit, Integer page) {
        return null;
    }
}

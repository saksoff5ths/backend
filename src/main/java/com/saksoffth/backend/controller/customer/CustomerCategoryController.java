package com.saksoffth.backend.controller.customer;

import java.util.List;

import com.saksoffth.backend.api.CustomerCategoryApi;
import com.saksoffth.backend.dto.CustomerCategoryDTO;
import com.saksoffth.backend.entity.Category;
import com.saksoffth.backend.mapper.CategoryMapper;
import com.saksoffth.backend.service.CategoryService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

/**
 * Main implementation of {@link CustomerCategoryApi}
 */
@RestController
@RequiredArgsConstructor
public class CustomerCategoryController implements CustomerCategoryApi {
    private final CategoryService categoryService;
    private final CategoryMapper categoryMapper;

    @Override
    public ResponseEntity<CustomerCategoryDTO> getCategoriesHierarchy(Long id) {
        var category = categoryService.getItem(id);
        var categoryDTO = categoryMapper.toCustomerDTO(category);
        var children = categoryService.getChildren(category);

        for (Category child : children) {
            categoryDTO.addChildrenItem(categoryMapper.toCustomerDTO(child));
        }
        return ResponseEntity.ok(categoryDTO);
    }

    @Override
    public ResponseEntity<List<CustomerCategoryDTO>> getParentCategories() {
        var categories = categoryService.getAllParentCategories();

        return ResponseEntity.ok(categories.stream()
                .map(categoryMapper::toCustomerDTO)
                .toList());
    }
}

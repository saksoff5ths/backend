package com.saksoffth.backend.controller.customer;

import java.util.List;

import com.saksoffth.backend.api.CustomerProductApi;
import com.saksoffth.backend.dto.CustomerProductDTO;
import com.saksoffth.backend.mapper.ProductMapper;
import com.saksoffth.backend.service.ProductService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

/**
 * Main implementation of {@link CustomerProductApi}
 */
@RestController
@RequiredArgsConstructor
public class CustomerProductController implements CustomerProductApi {
    private final ProductMapper productMapper;
    private final ProductService productService;

    @Override
    public ResponseEntity<List<CustomerProductDTO>> getCategoryProducts(Long categoryId, Integer limit) {
        var products = productService.getByCategoryId(categoryId, limit);

        return ResponseEntity.ok(products.stream()
                .map(productMapper::toCustomerDTO).toList());
    }

    @Override
    public ResponseEntity<List<CustomerProductDTO>> getProducts(Integer limit) {
        var products = productService.getLimited(limit);

        return ResponseEntity.ok(products.stream()
                .map(productMapper::toCustomerDTO)
                .toList());
    }
}

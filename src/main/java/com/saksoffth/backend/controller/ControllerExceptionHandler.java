package com.saksoffth.backend.controller;

import java.util.HashMap;
import java.util.Map;

import com.saksoffth.backend.dto.ErrorDTO;
import com.saksoffth.backend.exception.RecordNotFoundException;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

/**
 * General exception handler for Spring controllers.
 */
@Log4j2
@ControllerAdvice
public class ControllerExceptionHandler {

    /**
     * Handler for Internal server errors.
     *
     * @param e instance of {@link Exception}.
     * @return instance of {@link ErrorDTO}.
     */
    @ExceptionHandler(Exception.class)
    ResponseEntity<ErrorDTO> on(Exception e) {
        log.error("General Server Error", e);
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new ErrorDTO(500, "General error: " + e.getMessage(), null));
    }

    /**
     * Handler for exception {@link BadCredentialsException}.
     *
     * @param e intance of {@link BadCredentialsException}.
     * @return instance of {@link ErrorDTO}.
     */
    @ExceptionHandler(BadCredentialsException.class)
    ResponseEntity<ErrorDTO> on(BadCredentialsException e) {
        log.error("General Error", e);

        return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(new ErrorDTO(498, "Bad credentials", null));
    }

    /**
     * Handler for exception {@link MethodArgumentNotValidException}.
     *
     * @param e instance of {@link MethodArgumentNotValidException}.
     * @return instance of {@link ErrorDTO}.
     */
    @ExceptionHandler(MethodArgumentNotValidException.class)
    ResponseEntity<ErrorDTO> on(MethodArgumentNotValidException e) {
        Map<String, Object> meta = new HashMap<>();
        for (FieldError fieldError : e.getBindingResult().getFieldErrors()) {
            meta.put(fieldError.getField(), fieldError.getDefaultMessage());
        }
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ErrorDTO(499, "Validation error", meta));
    }

    /**
     * Handler for exception {@link RecordNotFoundException}.
     *
     * @param e instance of {@link RecordNotFoundException},
     * @return instance of {@link ErrorDTO}.
     */
    @ExceptionHandler(RecordNotFoundException.class)
    ResponseEntity<ErrorDTO> on(RecordNotFoundException e) {
        return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(new ErrorDTO(HttpStatus.UNAUTHORIZED.value(), e.getMessage(), null));
    }

    /**
     * Handler for exception {@link MethodArgumentTypeMismatchException}.
     *
     * @param e instance of {@link MethodArgumentTypeMismatchException}.
     * @return instance of {@link ErrorDTO}.
     */
    @ExceptionHandler(MethodArgumentTypeMismatchException.class)
    ResponseEntity<ErrorDTO> on(MethodArgumentTypeMismatchException e) {
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ErrorDTO(493, e.getValue() + " is not of type " + e.getRequiredType(), null));
    }
}

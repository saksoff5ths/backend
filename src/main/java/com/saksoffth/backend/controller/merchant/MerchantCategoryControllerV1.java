package com.saksoffth.backend.controller.merchant;

import java.util.List;
import java.util.stream.Collectors;
import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;

import com.saksoffth.backend.dto.CategoryDTO;
import com.saksoffth.backend.dto.SelectableDTO;
import com.saksoffth.backend.mapper.CategoryMapper;
import com.saksoffth.backend.service.CategoryService;
import com.saksoffth.backend.service.ProductService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

/**
 * Rest crud controller for {@link com.saksoffth.backend.entity.Category}.
 */
@RestController
@RequestMapping("/api/v1/merchant/category")
@RequiredArgsConstructor
@Tag(name = "Merchant category controller", description = "Controller for working with category.")
public class MerchantCategoryControllerV1 {

    private final ProductService productService;
    private final CategoryService categoryService;
    private final CategoryMapper categoryMapper;

    @GetMapping
    @ApiResponse(responseCode = "200")
    @Operation(summary = "Paginated list of categories", tags = {"Merchant Category Controller"})
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "200",
                    description = "List of categories",
                    content = @Content(
                            mediaType = MediaType.APPLICATION_JSON_VALUE,
                            schema = @Schema(implementation = Page.class, allOf = {CategoryDTO.class})
                    )
            )}
    )
    public Page<CategoryDTO> getItemList(@PageableDefault(sort = {"id"}, direction = Sort.Direction.DESC, size = 20) Pageable pageable) {
        var page = categoryService.getList(pageable);
        return page.map(categoryMapper::toDTO);
    }

    @GetMapping("/{id}")
    @Operation(summary = "Retrieves category item by id", tags = {"Merchant Category Controller"})
    @ApiResponse(responseCode = "200")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "200",
                    description = "Category item",
                    content = @Content(
                            mediaType = MediaType.APPLICATION_JSON_VALUE,
                            schema = @Schema(implementation = CategoryDTO.class)
                    )
            )}
    )
    public CategoryDTO getItem(@PathVariable Long id) {
        var category = categoryService.getItem(id);
        return categoryMapper.toDTO(category);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    @ApiResponse(responseCode = "201")
    @Operation(summary = "Creates category", tags = {"Merchant Category Controller"})
    public void createCategory(@Valid @RequestBody CategoryDTO categoryDTO) {
        var category = categoryMapper.fromDTO(categoryDTO);

        if (categoryDTO.parent() != null) {
            category.setParent(categoryService.getItem(category.getParent().getId()));
        }
        categoryService.saveItem(category);
    }

    @PutMapping("/{id}")
    @ApiResponse(responseCode = "200")
    @Operation(summary = "Updates category", tags = {"Merchant Category Controller"})
    public void updateCategory(@Valid @RequestBody CategoryDTO categoryDTO, @PathVariable Long id) {
        var category = categoryService.getItem(id);
        categoryMapper.fromDTO(category, categoryDTO);
        if (categoryDTO.parent() != null) {
            category.setParent(categoryService.getItem(category.getParent().getId()));
        }
        categoryService.saveItem(category);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @ApiResponse(responseCode = "204")
    @Operation(summary = "Deletes category", tags = {"Merchant Category Controller"})
    public void deleteCategory(@PathVariable Long id) {
        var category = categoryService.getItem(id);
        productService.deleteByCategory(category);

        categoryService.deleteItem(id);
    }

    @GetMapping("/selectable")
    @Operation(summary = "Get category list for select searching by name", tags = {"Merchant Category Controller"})
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "200",
                    description = "List of categories",
                    content = @Content(
                            mediaType = MediaType.APPLICATION_JSON_VALUE,
                            array = @ArraySchema(schema = @Schema(implementation = SelectableDTO.class))
                    )
            )}
    )
    public List<SelectableDTO> getForSelect(@RequestParam @NotEmpty String searchKey) {
        var category = categoryService.search(searchKey);
        return category.stream().map(categoryMapper::toSelectable).collect(Collectors.toList());
    }
}

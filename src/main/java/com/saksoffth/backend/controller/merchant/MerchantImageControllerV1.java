package com.saksoffth.backend.controller.merchant;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import com.saksoffth.backend.entity.Product;
import com.saksoffth.backend.service.ImageService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

/**
 * Rest crud controller for {@link Product}.
 */
@RestController
@RequestMapping("/api/v1/merchant/image")
@RequiredArgsConstructor
@Tag(name = "Merchant Image controller", description = "Controller for working with product images.")
public class MerchantImageControllerV1 {

    private final ImageService imageService;

    @GetMapping(value = "/{key}", produces = "*/*")
    @ApiResponse(responseCode = "200")
    @Operation(summary = "Retrieves image item by key", tags = {"Merchant Image controller"})
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "200",
                    description = "File resource",
                    content = @Content(
                            mediaType = MediaType.APPLICATION_JSON_VALUE,
                            schema = @Schema(implementation = FileSystemResource.class)
                    )
            )}
    )
    public void getItem(@PathVariable String key, HttpServletResponse response) {
        imageService.getItem(key, response);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    @ApiResponse(responseCode = "201")
    @Operation(summary = "Upload image and save", tags = {"Merchant Image controller"})
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "200",
                    description = "Image key",
                    content = @Content(
                            mediaType = MediaType.APPLICATION_JSON_VALUE,
                            schema = @Schema(implementation = String.class)
                    )
            )}
    )
    public String createItem(@Valid @RequestBody MultipartFile file) {
        return imageService.saveFile(file);
    }

    @DeleteMapping("/{key}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @ApiResponse(responseCode = "204")
    @Operation(summary = "Deletes image", tags = {"Merchant Image controller"})
    public void deleteItem(@PathVariable String key) {
        imageService.deleteFile(key);
    }
}

package com.saksoffth.backend.controller.merchant;

import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/merchant/import")
@RequiredArgsConstructor
@Tag(name = "Merchant Import controller", description = "Controller for working with product import.")
public class MerchantImportControllerV1 {
}

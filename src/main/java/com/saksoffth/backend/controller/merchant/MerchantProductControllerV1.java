package com.saksoffth.backend.controller.merchant;

import javax.validation.Valid;

import com.saksoffth.backend.dto.CategoryDTO;
import com.saksoffth.backend.dto.ProductDTO;
import com.saksoffth.backend.entity.Product;
import com.saksoffth.backend.mapper.ProductMapper;
import com.saksoffth.backend.service.ProductService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

/**
 * Rest crud controller for {@link Product}.
 */
@RestController
@RequestMapping("/api/v1/merchant/product")
@RequiredArgsConstructor
@Tag(name = "Merchant product controller", description = "Controller for working with product.")
public class MerchantProductControllerV1 {

    private final ProductService productService;
    private final ProductMapper productMapper;

    @GetMapping
    @ApiResponse(responseCode = "200")
    @Operation(
            summary = "Paginated list of products",
            tags = {"Merchant product controller"})
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "200",
                    description = "List of Products",
                    content = @Content(
                            mediaType = MediaType.APPLICATION_JSON_VALUE,
                            schema = @Schema(implementation = Page.class, allOf = {ProductDTO.class})
                    )
            )}
    )
    public Page<ProductDTO> getItemList(@PageableDefault(sort = {"id"}, direction = Sort.Direction.DESC, size = 20) Pageable pageable) {
        var page = productService.getList(pageable);
        return page.map(productMapper::toDTO);
    }

    @GetMapping("/{id}")
    @ApiResponse(responseCode = "200")
    @Operation(summary = "Retrieves product item by id", tags = {"Merchant product controller"})
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "200",
                    description = "Product item",
                    content = @Content(
                            mediaType = MediaType.APPLICATION_JSON_VALUE,
                            schema = @Schema(implementation = ProductDTO.class)
                    )
            )}
    )
    public ProductDTO getItem(@PathVariable Long id) {
        var product = productService.getItem(id);
        return productMapper.toDTO(product);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    @ApiResponse(responseCode = "201")
    @Operation(summary = "Creates product", tags = {"Merchant product controller"})
    public void createItem(@Valid @RequestBody ProductDTO productDTO) {
        var product = productMapper.fromDTO(productDTO);
        productService.saveItem(product);
    }

    @PutMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    @ApiResponse(responseCode = "200")
    @Operation(summary = "Updates product", tags = {"Merchant product controller"})
    public void updateItem(@Valid @RequestBody ProductDTO productDTO, @PathVariable Long id) {
        var product = productService.getItem(id);
        productMapper.fromDTO(product, productDTO);
        productService.saveItem(product);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @ApiResponse(responseCode = "204")
    @Operation(summary = "Deletes product", tags = {"Merchant product controller"})
    public void deleteItem(@PathVariable Long id) {
        productService.deleteItem(id);
    }
}

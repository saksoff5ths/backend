package com.saksoffth.backend.controller.merchant;

import javax.validation.Valid;

import com.saksoffth.backend.dto.ProductDTO;
import com.saksoffth.backend.dto.VendorDTO;
import com.saksoffth.backend.mapper.VendorMapper;
import com.saksoffth.backend.service.ProductService;
import com.saksoffth.backend.service.VendorService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

/**
 * Rest crud controller for {@link com.saksoffth.backend.entity.Vendor}.
 */
@RestController
@RequestMapping("/api/v1/merchant/vendor")
@RequiredArgsConstructor
@Tag(name = "Merchant vendor controller", description = "Controller for working with category.")
public class MerchantVendorControllerV1 {

    private final ProductService productService;
    private final VendorService vendorService;
    private final VendorMapper vendorMapper;

    @GetMapping
    @ApiResponse(responseCode = "200")
    @Operation(
            summary = "Paginated list of vendors",
            tags = {"Merchant vendor controller"})
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "200",
                    description = "List of vendors",
                    content = @Content(
                            mediaType = MediaType.APPLICATION_JSON_VALUE,
                            schema = @Schema(implementation = Page.class, allOf = {VendorDTO.class})
                    )
            )}
    )
    public Page<VendorDTO> getItemList(@PageableDefault(sort = {"id"}, direction = Sort.Direction.DESC, size = 20) Pageable pageable) {
        var page = vendorService.getList(pageable);
        return page.map(vendorMapper::toDTO);
    }

    @GetMapping("/{id}")
    @ApiResponse(responseCode = "200")
    @Operation(summary = "Retrieves vendor item by id", tags = {"Merchant vendor controller"})
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "200",
                    description = "Vendor item",
                    content = @Content(
                            mediaType = MediaType.APPLICATION_JSON_VALUE,
                            schema = @Schema(implementation = VendorDTO.class)
                    )
            )}
    )
    public VendorDTO getItem(@PathVariable Long id) {
        var vendor = vendorService.getItem(id);
        return vendorMapper.toDTO(vendor);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    @ApiResponse(responseCode = "201")
    @Operation(summary = "Creates vendor", tags = {"Merchant vendor controller"})
    public void createItem(@Valid @RequestBody VendorDTO vendorDTO) {
        var vendor = vendorMapper.fromDTO(vendorDTO);
        vendorService.saveItem(vendor);
    }

    @PutMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    @ApiResponse(responseCode = "200")
    @Operation(summary = "Updates vendor", tags = {"Merchant vendor controller"})
    public void updateItem(@Valid @RequestBody VendorDTO vendorDTO, @PathVariable Long id) {
        var vendor = vendorService.getItem(id);
        vendorMapper.fromDTO(vendor, vendorDTO);
        vendorService.saveItem(vendor);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @ApiResponse(responseCode = "204")
    @Operation(summary = "Deletes vendor", tags = {"Merchant vendor controller"})
    public void deleteItem(@PathVariable Long id) {
        var vendor = vendorService.getItem(id);
        productService.deleteByVendor(vendor);

        vendorService.deleteItem(id);
    }
}

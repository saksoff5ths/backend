package com.saksoffth.backend.controller.merchant;

import javax.validation.Valid;

import com.saksoffth.backend.dto.PasswordDTO;
import com.saksoffth.backend.dto.UserDTO;
import com.saksoffth.backend.entity.User;
import com.saksoffth.backend.entity.UserStatus;
import com.saksoffth.backend.mapper.UserMapper;
import com.saksoffth.backend.service.UserService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

/**
 * Rest crud controller for {@link com.saksoffth.backend.entity.Category}.
 */
@RestController
@RequestMapping("/api/v1/merchant/user")
@RequiredArgsConstructor
@Tag(name = "Merchant user controller", description = "Controller for working with users.")
public class MerchantUserControllerV1 {

    private final UserService userService;
    private final UserMapper userMapper;
    private final PasswordEncoder passwordEncoder;

    @GetMapping
    @ApiResponse(responseCode = "200")
    @Operation(
            summary = "Paginated list of users",
            tags = {"Merchant user controller"})
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "200",
                    description = "List of users",
                    content = @Content(
                            mediaType = MediaType.APPLICATION_JSON_VALUE,
                            schema = @Schema(implementation = Page.class, allOf = {UserDTO.class})
                    )
            )}
    )
    public Page<UserDTO> getUserList(@PageableDefault(sort = {"id"}, direction = Sort.Direction.DESC, size = 20) Pageable pageable) {
        var page = userService.getList(pageable);
        return page.map(userMapper::toDTO);
    }

    @GetMapping("/{id}")
    @ApiResponse(responseCode = "200")
    @Operation(summary = "Retrieves user item by id", tags = {"Merchant user controller"})
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "200",
                    description = "User item",
                    content = @Content(
                            mediaType = MediaType.APPLICATION_JSON_VALUE,
                            schema = @Schema(implementation = UserDTO.class)
                    )
            )}
    )
    public UserDTO getUser(@PathVariable Long id) {
        var user = userService.getUser(id);
        return userMapper.toDTO(user);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    @ApiResponse(responseCode = "201")
    @Operation(summary = "Creates user", tags = {"Merchant user controller"})
    public void createUser(@Valid @RequestBody UserDTO userDTO) {
        var user = userMapper.fromDTO(userDTO);

        userService.saveUser(user);
    }

    @PutMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    @ApiResponse(responseCode = "200")
    @Operation(summary = "Updates user by id", tags = {"Merchant user controller"})
    public void updateUser(@Valid @RequestBody UserDTO userDTO, @PathVariable Long id) {
        var user = userService.getUser(id);
        userMapper.fromDTO(user, userDTO);

        userService.saveUser(user);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @ApiResponse(responseCode = "204")
    @Operation(summary = "Deletes user by id", tags = {"Merchant user controller"})
    public void deleteItem(@PathVariable Long id) {
        userService.deleteUser(id);
    }

    @PutMapping("/{id}/password")
    @ResponseStatus(HttpStatus.OK)
    @ApiResponse(responseCode = "200")
    @Operation(summary = "Set user password by id", tags = {"Merchant user controller"})
    public void setPassword(@Valid @RequestBody PasswordDTO passwordDTO, @PathVariable Long id) {
        var user = userService.getUser(id);
        var encodedPassword = passwordEncoder.encode(passwordDTO.password());
        userMapper.fromDTO(user, encodedPassword);

        userService.saveUser(user);
    }

    @PutMapping("/{id}/activate")
    @ResponseStatus(HttpStatus.OK)
    @ApiResponse(responseCode = "200")
    @Operation(summary = "Set user status to 'ACTIVE", tags = {"Merchant user controller"})
    public void setActive(@PathVariable Long id) {
        var user = userService.getUser(id);
        userMapper.fromDTO(user, UserStatus.ACTIVE);

        userService.saveUser(user);
    }

    @PutMapping("/{id}/block")
    @ResponseStatus(HttpStatus.OK)
    @ApiResponse(responseCode = "200")
    @Operation(summary = "Set user status to 'ACTIVE", tags = {"Merchant user controller"})
    public void setBlocked(@PathVariable Long id) {
        var user = userService.getUser(id);
        userMapper.fromDTO(user, UserStatus.BLOCKED);

        userService.saveUser(user);
    }
}

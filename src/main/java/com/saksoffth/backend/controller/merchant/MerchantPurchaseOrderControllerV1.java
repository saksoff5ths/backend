package com.saksoffth.backend.controller.merchant;

import com.saksoffth.backend.dto.ProductDTO;
import com.saksoffth.backend.dto.PurchaseOrderDTO;
import com.saksoffth.backend.mapper.PurchaseOrderMapper;
import com.saksoffth.backend.service.PurchaseOrderService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Rest crud controller for {@link com.saksoffth.backend.entity.Vendor}.
 */
@RestController
@RequestMapping("/api/v1/merchant/purchase-order")
@RequiredArgsConstructor
@Tag(name = "Merchant purchase order controller", description = "Controller for working with purchase order.")
public class MerchantPurchaseOrderControllerV1 {

    private final PurchaseOrderService purchaseOrderService;
    private final PurchaseOrderMapper purchaseOrderMapper;

    @GetMapping
    @ApiResponse(responseCode = "200")
    @Operation(
            summary = "Paginated list of purchase orders",
            tags = {"Merchant purchase order controller"})
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "200",
                    description = "List of purchase orders",
                    content = @Content(
                            mediaType = MediaType.APPLICATION_JSON_VALUE,
                            schema = @Schema(implementation = Page.class, allOf = {PurchaseOrderDTO.class})
                    )
            )}
    )
    public Page<PurchaseOrderDTO> getItemList(@PageableDefault(sort = {"id"}, direction = Sort.Direction.DESC, size = 20) Pageable pageable) {
        var page = purchaseOrderService.getList(pageable);
        return page.map(purchaseOrderMapper::toDTO);
    }

    @GetMapping("/{id}")
    @ApiResponse(responseCode = "200")
    @Operation(summary = "Retrieves purchase order item by id", tags = {"Merchant purchase order controller"})
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "200",
                    description = "Purchase order item",
                    content = @Content(
                            mediaType = MediaType.APPLICATION_JSON_VALUE,
                            schema = @Schema(implementation = PurchaseOrderDTO.class)
                    )
            )}
    )
    public PurchaseOrderDTO getItem(@PathVariable Long id) {
        var vendor = purchaseOrderService.getItem(id);
        return purchaseOrderMapper.toDTO(vendor);
    }

/*    @PutMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public void updateItem(@Valid @RequestBody VendorDTO vendorDTO, @PathVariable Long id) {
        var vendor = purchaseOrderService.getItem(id);
        purchaseOrderMapper.fromDTO(vendor, vendorDTO);
        purchaseOrderService.saveItem(vendor);
    }*/
}

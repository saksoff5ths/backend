package com.saksoffth.backend.utils;

import lombok.SneakyThrows;
import lombok.experimental.UtilityClass;
import org.im4java.core.ConvertCmd;
import org.im4java.core.IMOperation;

@UtilityClass
public class ImageUtils {

    @SneakyThrows
    public static String convertImage(String path, String name, int height, int width) {
        var filePath = path + "resized+" + name;

        var imOperation = new IMOperation();
        imOperation.addImage(path + name);
        imOperation.resize(height, width);
        imOperation.addImage(filePath);

        new ConvertCmd().run(imOperation);
        return filePath;
    }
}

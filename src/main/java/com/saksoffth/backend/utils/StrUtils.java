package com.saksoffth.backend.utils;

import org.springframework.util.StringUtils;

public class StrUtils {

    public static String getSearchText(String txt) {
        if (!StringUtils.hasText(txt)) {
            return txt;
        }
        return "%" + txt.toLowerCase() + "%";
    }
}

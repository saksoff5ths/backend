package com.saksoffth.backend.config.security;

import java.util.Date;
import java.util.Optional;

import com.saksoffth.backend.dto.UserDetailsDTO;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.MalformedJwtException;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.SignatureException;
import io.jsonwebtoken.UnsupportedJwtException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

/**
 * Jwt token provider class.
 */
@Slf4j
@Component
public class JwtTokenProvider {

    @Value("${app.authentication.jwtSecret}")
    private String jwtSecret;
    @Value("${app.authentication.jwtExpirationInMs}")
    private long jwtExpirationInMs;

    /**
     * Generates jwt token string.
     *
     * @param authentication instance of {@link Authentication}
     * @return jwt token string
     */
    public String generateToken(Authentication authentication) {
        return this.generateTokenWithExpireTime(authentication, jwtExpirationInMs);
    }

    /**
     * Generates jwt token string.
     *
     * @param authentication instance of {@link Authentication}
     * @param expireTimeInMs expire time in millisecond.
     * @return jwt token string
     */
    private String generateTokenWithExpireTime(Authentication authentication, Long expireTimeInMs) {
        var userPrincipal = (UserDetailsDTO) authentication.getPrincipal();
        var now = new Date();
        var expiryDate = new Date(now.getTime() + Optional.ofNullable(expireTimeInMs).orElse(0L));

        return Jwts.builder()
                .setSubject(userPrincipal.getEmail())
                .setIssuedAt(new Date())
                .setExpiration(expiryDate)
                .signWith(SignatureAlgorithm.HS512, jwtSecret)
                .setIssuer("saksoff5th")
                .compact();
    }

    String getUsernameFromJWT(String token) {
        return Jwts.parser()
                .setSigningKey(jwtSecret)
                .parseClaimsJws(token)
                .getBody()
                .getSubject();
    }

    boolean validateToken(String authToken) {
        try {
            Jwts.parser().setSigningKey(jwtSecret).parseClaimsJws(authToken);
            return true;
        } catch (SignatureException ex) {
            log.error("Invalid JWT signature");
        } catch (MalformedJwtException ex) {
            log.error("Invalid JWT token");
        } catch (ExpiredJwtException ex) {
            log.error("Expired JWT token");
        } catch (UnsupportedJwtException ex) {
            log.error("Unsupported JWT token");
        } catch (IllegalArgumentException ex) {
            log.error("JWT claims string is empty.");
        }
        return false;
    }
}

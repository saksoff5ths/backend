package com.saksoffth.backend.config.security;

import javax.servlet.FilterChain;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.saksoffth.backend.mapper.UserMapper;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.util.StringUtils;
import org.springframework.web.filter.OncePerRequestFilter;

/**
 * Jwt Authentication Filter.
 */
@Slf4j
@RequiredArgsConstructor
public class JwtAuthenticationFilter extends OncePerRequestFilter {
    private static final String TOKEN_START_TEXT = "Bearer ";

    private final UserMapper userMapper;
    private final JwtTokenProvider tokenProvider;
    private final UserDetailsService userDetailService;

    @Override
    @SneakyThrows
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) {
        var jwt = this.getJwtFromRequest(request);

        if (StringUtils.hasText(jwt) && tokenProvider.validateToken(jwt)) {
            var username = tokenProvider.getUsernameFromJWT(jwt);
            var userDetails = userDetailService.loadUserByUsername(username);

            SecurityContextHolder.getContext().setAuthentication(userMapper.toAuthToken(userDetails, request));
        }
        filterChain.doFilter(request, response);
    }

    private String getJwtFromRequest(HttpServletRequest request) {
        var bearerToken = request.getHeader("Authorization");

        if (StringUtils.hasText(bearerToken) && bearerToken.startsWith(TOKEN_START_TEXT)) {
            return bearerToken.substring(TOKEN_START_TEXT.length());
        }
        return null;
    }
}
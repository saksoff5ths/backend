package com.saksoffth.backend.config.security;

import com.saksoffth.backend.dto.UserDetailsDTO;
import com.saksoffth.backend.mapper.UserMapper;
import com.saksoffth.backend.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

/**
 * Main implementation of {@link UserDetailsService}.
 */
@Service
@RequiredArgsConstructor
public class UserDetailServiceImpl implements UserDetailsService {

    private final UserMapper userMapper;
    private final UserRepository userRepository;

    /**
     * Load {@link UserDetails} by username (email).
     *
     * @param username email of user.
     * @return instance of {@link UserDetails}
     * @throws UsernameNotFoundException throws {@link UsernameNotFoundException}.
     */
    @Override
    public UserDetailsDTO loadUserByUsername(String username) throws UsernameNotFoundException {
        var user = userRepository.findByEmail(username)
                .orElseThrow(() -> new UsernameNotFoundException(String.format("User with email %s not found.", username)));

        return userMapper.toUserDetailsDTO(user);
    }
}

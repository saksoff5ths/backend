package com.saksoffth.backend.config;

import java.net.URI;
import java.net.URISyntaxException;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import software.amazon.awssdk.regions.Region;
import software.amazon.awssdk.services.s3.S3Client;

/**
 * AWS s3 configuration.
 */
@Configuration
public class AwsS3Configuration {

    @Value("${aws.local.endpoint}")
    private String endpoint;

    @Bean
    public S3Client awsS3Client() throws URISyntaxException {
        return S3Client.builder()
                .region(Region.US_EAST_1)
                .endpointOverride(new URI(endpoint))
                .build();
    }
}

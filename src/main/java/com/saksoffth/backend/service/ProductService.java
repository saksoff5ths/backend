package com.saksoffth.backend.service;

import java.util.List;
import javax.validation.constraints.NotNull;

import com.saksoffth.backend.entity.Category;
import com.saksoffth.backend.entity.Product;
import com.saksoffth.backend.entity.Vendor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

/**
 * Contains business logic for working with domain {@link Product}.
 */
public interface ProductService {

    /**
     * Delete all product by {@link Category}.
     *
     * @param category instance of {@link Category}
     */
    void deleteByCategory(@NotNull Category category);

    /**
     * Delete all product by {@link Vendor}.
     *
     * @param vendor instance of {@link Vendor}.
     */
    void deleteByVendor(Vendor vendor);

    /**
     * Method for returning {@link Product} paginated list.
     *
     * @param pageable parameter.
     * @return {@link Page<Product>}.
     */
    Page<Product> getList(Pageable pageable);

    /**
     * Retrieves {@link Product} by id from DB.
     *
     * @param id of category in DB.
     * @return {@link Product}.
     */
    Product getItem(Long id);

    /**
     * Save {@link Product} in DB.
     *
     * @param product instance of {@link Product}.
     */
    void saveItem(Product product);

    /**
     * Delete product by id.
     *
     * @param id of Product.
     */
    void deleteItem(Long id);

    /**
     * Get list of products limited.
     *
     * @param limit limit.
     * @return instance of {@link Product}
     */
    List<Product> getLimited(int limit);

    /**
     * Get list of products by categoryId limited.
     *
     * @param limit limit.
     * @return instance of {@link Product}
     */
    List<Product> getByCategoryId(Long categoryId, int limit);
}

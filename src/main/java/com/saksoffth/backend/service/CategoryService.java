package com.saksoffth.backend.service;

import java.util.List;

import com.saksoffth.backend.entity.Category;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Contains business logic for working with domain {@link Category}.
 */
public interface CategoryService {
    /**
     * Method for returning {@link Category} paginated list.
     *
     * @param pageable parameter.
     * @return {@link Page<Category>}.
     */
    Page<Category> getList(Pageable pageable);

    /**
     * Retrieves {@link Category} by id from DB.
     *
     * @param id of user in DB.
     * @return {@link Category}.
     */
    Category getItem(Long id);

    /**
     * Save {@link Category} in DB.
     *
     * @param category instance of {@link Category}.
     */
    void saveItem(Category category);

    /**
     * Delete category by id.
     *
     * @param id of Category.
     */
    void deleteItem(Long id);

    /**
     * Search for category by name.
     *
     * @param searchKey key for search in name field.
     * @return {@link List<Category>}.
     */
    List<Category> search(String searchKey);

    /**
     * Returns all
     *
     * @return instance of {@link List<Category>}.
     */
    List<Category> getAllParentCategories();

    /**
     * Returns children of given category.
     *
     * @param category instance of {@link Category}
     * @return instance of {@link List<Category>}
     */
    List<Category> getChildren(Category category);
}

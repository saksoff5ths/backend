package com.saksoffth.backend.service;

import java.nio.file.Path;
import javax.validation.constraints.NotEmpty;

import software.amazon.awssdk.core.ResponseInputStream;
import software.amazon.awssdk.services.s3.model.GetObjectResponse;

/**
 * File storage service.
 */
public interface FileStorageService {

    /**
     * Returns AWS S3 file by key.
     *
     * @param key File key stored in AWS S3.
     * @return instance of {@link ResponseInputStream<GetObjectResponse>}
     */
    ResponseInputStream<GetObjectResponse> getFile(@NotEmpty String key);

    /**
     * Get File full url.
     *
     * @param key file key.
     * @return file full url.
     */
    String getUrl(String key);

    /**
     * Save file to AWS S3 service.
     *
     * @param path instance of {@link Path}
     * @param key  file key.
     */
    void saveFile(Path path, String key);

    /**
     * Delete file from AWS S3 storage by key.
     *
     * @param key file key.
     */
    void deleteByKey(String key);
}

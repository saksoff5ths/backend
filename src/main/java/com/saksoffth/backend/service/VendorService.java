package com.saksoffth.backend.service;


import com.saksoffth.backend.entity.Vendor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Contains business logic for working with domain {@link Vendor}.
 */
public interface VendorService {
    /**
     * Method for returning {@link Vendor} paginated list.
     *
     * @param pageable parameter.
     * @return {@link Page<Vendor>}.
     */
    Page<Vendor> getList(Pageable pageable);

    /**
     * Retrieves {@link Vendor} by id from DB.
     *
     * @param id of user in DB.
     * @return {@link Vendor}.
     */
    Vendor getItem(Long id);

    /**
     * Save {@link Vendor} in DB.
     *
     * @param vendor instance of {@link Vendor}.
     */
    void saveItem(Vendor vendor);

    /**
     * Delete vendor by id.
     *
     * @param id of Vendor.
     */
    void deleteItem(Long id);
}

package com.saksoffth.backend.service;

import com.saksoffth.backend.entity.PurchaseOrder;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Contains business logic for working with domain {@link PurchaseOrder}.
 */
public interface PurchaseOrderService {

    /**
     * Method for returning {@link PurchaseOrder} paginated list.
     *
     * @param pageable parameter.
     * @return {@link Page<PurchaseOrder>}.
     */
    Page<PurchaseOrder> getList(Pageable pageable);

    /**
     * Retrieves {@link PurchaseOrder} by id from DB.
     *
     * @param id of user in DB.
     * @return {@link PurchaseOrder}.
     */

    PurchaseOrder getItem(Long id);

}

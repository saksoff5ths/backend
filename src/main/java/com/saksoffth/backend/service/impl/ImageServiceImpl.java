package com.saksoffth.backend.service.impl;

import java.nio.file.Files;
import java.nio.file.Path;
import java.util.UUID;
import javax.servlet.http.HttpServletResponse;

import com.saksoffth.backend.service.FileStorageService;
import com.saksoffth.backend.service.ImageService;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.log4j.Log4j2;
import org.im4java.core.ConvertCmd;
import org.im4java.core.IMOperation;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

/**
 * Main implementation of {@link ImageService}.
 */
@Log4j2
@Service
@RequiredArgsConstructor
public class ImageServiceImpl implements ImageService {

    private final FileStorageService fileStorageService;

    @Value("${app.image.height:200}")
    private Integer height;
    @Value("${app.image.width:150}")
    private Integer width;

    @Override
    @SneakyThrows
    public void getItem(String key, HttpServletResponse response) {
        var s3Response = fileStorageService.getFile(key);

        response.getOutputStream().write(s3Response.readAllBytes());
    }

    @Override
    public String saveFile(MultipartFile file) {
        var key = UUID.randomUUID();
        var path = resizeImage(file);
        fileStorageService.saveFile(path, key.toString());
        return fileStorageService.getUrl(key.toString());
    }

    @Override
    public void deleteFile(String key) {
        fileStorageService.deleteByKey(key);
    }

    @SneakyThrows
    private Path resizeImage(MultipartFile file) {
        var tempFile = Files.createTempFile(null, StringUtils.getFilenameExtension(file.getOriginalFilename()));

        Files.write(tempFile, file.getBytes());
        var imOperation = new IMOperation();
        imOperation.addImage(tempFile.toAbsolutePath().normalize().toString());
        imOperation.resize(height, width);

        var destinationFile = Files.createTempFile(null, StringUtils.getFilenameExtension(file.getOriginalFilename()));
        imOperation.addImage(destinationFile.toAbsolutePath().normalize().toString());

        var imageCommand = new ConvertCmd();
        //full search path is set here, because last versions of image magick has issues on window 10 and 11 machines.
        imageCommand.setSearchPath("C:\\Program Files\\ImageMagick-7.1.0-Q16");
        imageCommand.run(imOperation);
        return destinationFile;
    }
}

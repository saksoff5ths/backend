package com.saksoffth.backend.service.impl;

import javax.validation.constraints.NotNull;
import java.util.List;

import com.saksoffth.backend.entity.Category;
import com.saksoffth.backend.entity.Product;
import com.saksoffth.backend.entity.Vendor;
import com.saksoffth.backend.exception.RecordNotFoundException;
import com.saksoffth.backend.repository.ProductRepository;
import com.saksoffth.backend.service.ProductService;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

/**
 * Main implementation of {@link ProductService}.
 */
@Log4j2
@Service
@RequiredArgsConstructor
public class ProductServiceImpl implements ProductService {
    private final ProductRepository productRepository;

    @Override
    public void deleteByCategory(@NotNull Category category) {
        productRepository.deleteAllByCategory(category);
    }

    @Override
    public void deleteByVendor(@NotNull Vendor vendor) {
        productRepository.deleteAllByVendor(vendor);
    }

    @Override
    public Page<Product> getList(Pageable pageable) {
        return productRepository.findAll(pageable);
    }

    @Override
    public Product getItem(Long id) {
        return productRepository.findById(id).orElseThrow(() -> new RecordNotFoundException("Product", id));
    }

    @Override
    public void saveItem(Product product) {
        productRepository.save(product);
    }

    @Override
    public void deleteItem(Long id) {
        productRepository.deleteById(id);
    }

    @Override
    public List<Product> getLimited(int limit) {
        return productRepository.findAllActive(Pageable.ofSize(limit));
    }

    @Override
    public List<Product> getByCategoryId(Long categoryId, int limit) {
        return productRepository.findAllActiveAndCategoryId(categoryId, Pageable.ofSize(limit));
    }
}

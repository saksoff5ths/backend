package com.saksoffth.backend.service.impl;

import com.saksoffth.backend.entity.PurchaseOrder;
import com.saksoffth.backend.repository.PurchaseOrderRepository;
import com.saksoffth.backend.service.PurchaseOrderService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

/**
 * Main implementation of {@link PurchaseOrderService}.
 */
@Service
@RequiredArgsConstructor
public class PurchaseOrderServiceImpl implements PurchaseOrderService {
    private final PurchaseOrderRepository purchaseOrderRepository;

    @Override
    public Page<PurchaseOrder> getList(Pageable pageable) {
        return purchaseOrderRepository.findAll(pageable);
    }

    @Override
    public PurchaseOrder getItem(Long id) {
        return null;
    }
}

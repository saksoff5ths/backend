package com.saksoffth.backend.service.impl;

import java.nio.file.Path;
import javax.validation.constraints.NotEmpty;

import com.saksoffth.backend.service.FileStorageService;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import software.amazon.awssdk.core.ResponseInputStream;
import software.amazon.awssdk.core.sync.RequestBody;
import software.amazon.awssdk.services.s3.S3Client;
import software.amazon.awssdk.services.s3.model.CreateBucketRequest;
import software.amazon.awssdk.services.s3.model.DeleteObjectRequest;
import software.amazon.awssdk.services.s3.model.GetObjectRequest;
import software.amazon.awssdk.services.s3.model.GetObjectResponse;
import software.amazon.awssdk.services.s3.model.GetUrlRequest;
import software.amazon.awssdk.services.s3.model.HeadBucketRequest;
import software.amazon.awssdk.services.s3.model.NoSuchBucketException;
import software.amazon.awssdk.services.s3.model.PutObjectRequest;

/**
 * Main implementation of {@link FileStorageService} for saving in AWS S3.
 */
@Log4j2
@Service
@RequiredArgsConstructor
public class AwsFileStorageServiceImpl implements FileStorageService {
    private final S3Client s3Client;
    @Value("${aws.local.bucket}")
    private String bucket;

    @Override
    public ResponseInputStream<GetObjectResponse> getFile(@NotEmpty String key) {
        return s3Client.getObject(GetObjectRequest.builder()
                .bucket(bucket)
                .key(key)
                .build());
    }

    public String getUrl(String key) {
        return s3Client.utilities().getUrl(GetUrlRequest.builder()
                .key(key)
                .bucket(bucket)
                .build()).toExternalForm();
    }

    @Override
    public void saveFile(Path path, String key) {
        checkBucketOrCreateBucket();
        var request = PutObjectRequest.builder()
                .bucket(bucket)
                .key(key)
                .build();

        s3Client.putObject(request, RequestBody.fromFile(path));
    }

    @Override
    public void deleteByKey(String key) {
        var request = DeleteObjectRequest.builder()
                .bucket(bucket)
                .key(key)
                .build();

        s3Client.deleteObject(request);
    }

    private void checkBucketOrCreateBucket() {
        try {
            s3Client.headBucket(HeadBucketRequest.builder().bucket(bucket).build());
        } catch (NoSuchBucketException e) {
            log.info("Bucket: {} doesn't exist. Create new bucket", bucket);
            s3Client.createBucket(CreateBucketRequest.builder().bucket(bucket).build());
        }
    }
}

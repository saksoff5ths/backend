package com.saksoffth.backend.service.impl;

import com.saksoffth.backend.entity.Vendor;
import com.saksoffth.backend.exception.RecordNotFoundException;
import com.saksoffth.backend.repository.VendorRepository;
import com.saksoffth.backend.service.VendorService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

/**
 * Main implementation of {@link VendorService}.
 */
@Service
@RequiredArgsConstructor
public class VendorServiceImpl implements VendorService {
    private final VendorRepository vendorRepository;

    @Override
    public Page<Vendor> getList(Pageable pageable) {
        return vendorRepository.findAll(pageable);
    }

    @Override
    public Vendor getItem(Long id) {
        return vendorRepository.findById(id).orElseThrow(() -> new RecordNotFoundException("Vendor", id));
    }

    @Override
    public void saveItem(Vendor vendor) {
        vendorRepository.save(vendor);
    }

    @Override
    public void deleteItem(Long id) {
        vendorRepository.deleteById(id);
    }
}

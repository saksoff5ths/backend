package com.saksoffth.backend.service.impl;

import com.saksoffth.backend.service.FileStorageService;
import com.saksoffth.backend.service.ImportService;
import com.saksoffth.backend.service.ProductService;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;

/**
 * Main implementation of {@link ImportService}.
 */
@Log4j2
@Service
@RequiredArgsConstructor
public class ImportServiceImpl implements ImportService {
    private final ProductService productService;
    private final FileStorageService fileStorageService;


}

package com.saksoffth.backend.service.impl;

import java.util.List;

import com.saksoffth.backend.entity.Category;
import com.saksoffth.backend.exception.RecordNotFoundException;
import com.saksoffth.backend.repository.CategoryRepository;
import com.saksoffth.backend.service.CategoryService;
import com.saksoffth.backend.utils.StrUtils;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

/**
 * Main implementation of {@link CategoryService}.
 */
@Log4j2
@Service
@RequiredArgsConstructor
public class CategoryServiceImpl implements CategoryService {

    private final CategoryRepository categoryRepository;

    @Override
    public Page<Category> getList(Pageable pageable) {
        return categoryRepository.findAll(pageable);
    }

    @Override
    public Category getItem(Long id) {
        return categoryRepository.findById(id).orElseThrow(() -> new RecordNotFoundException("Category", id));
    }

    @Override
    public void saveItem(Category category) {
        categoryRepository.save(category);
    }

    @Override
    public void deleteItem(Long id) {
        categoryRepository.deleteById(id);
    }

    @Override
    public List<Category> search(String searchKey) {
        return categoryRepository.findAllByNameLike(StrUtils.getSearchText(searchKey));
    }

    @Override
    public List<Category> getAllParentCategories() {
        return categoryRepository.findAllByParentIsNull();
    }

    @Override
    public List<Category> getChildren(Category category) {
        return categoryRepository.findAllByParent(category);
    }
}

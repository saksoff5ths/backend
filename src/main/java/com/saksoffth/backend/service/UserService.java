package com.saksoffth.backend.service;

import com.saksoffth.backend.entity.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Contains business logic for working with domain {@link User}.
 */
public interface UserService {

    /**
     * Method for returning {@link User} paginated list.
     *
     * @param pageable parameter.
     * @return {@link Page<User>}.
     */
    Page<User> getList(Pageable pageable);

    /**
     * Retrieves {@link User} by id from DB.
     *
     * @param id of user in DB.
     * @return {@link User}.
     */
    User getUser(Long id);

    /**
     * Save {@link User}.
     *
     * @param user instance of {@link User}
     */
    void saveUser(User user);

    /**
     * Deleted user.
     *
     * @param id of dat_user table.
     */
    void deleteUser(Long id);
}

package com.saksoffth.backend.service;

import javax.servlet.http.HttpServletResponse;

import org.springframework.web.multipart.MultipartFile;

/**
 * Image service.
 */
public interface ImageService {

    /**
     * Returns file by key.
     *
     * @param key      file key.
     * @param response instance of {@link HttpServletResponse}.
     */
    void getItem(String key, HttpServletResponse response);

    /**
     * Resizes image and uploads file to AWS S3 storage.
     *
     * @param file instance of {@link MultipartFile}.
     * @return key of uploaded file.
     */
    String saveFile(MultipartFile file);

    /**
     * Delete image file by key.
     *
     * @param key AWS file key.
     */
    void deleteFile(String key);
}

package com.saksoffth.backend.mapper;

import com.saksoffth.backend.dto.VendorDTO;
import com.saksoffth.backend.entity.Vendor;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;

/**
 * Mapper for {@link Vendor} domain entity.
 */
@Mapper(componentModel = "spring")
public interface VendorMapper {

    /**
     * Maps {@link VendorDTO} from {@link Vendor}.
     *
     * @param vendor instance of {@link Vendor}
     * @return instance of {@link VendorDTO}
     */
    VendorDTO toDTO(Vendor vendor);

    /**
     * Maps {@link Vendor} from {@link VendorDTO}.
     *
     * @param vendorDTO instance of {@link VendorDTO}
     */
    @Mapping(target = "id", ignore = true)
    Vendor fromDTO(VendorDTO vendorDTO);

    /**
     * Maps {@link Vendor} from {@link VendorDTO}.
     *
     * @param vendor    instance of {@link Vendor}
     * @param vendorDTO instance of {@link VendorDTO}
     */
    @Mapping(target = "id", ignore = true)
    void fromDTO(@MappingTarget Vendor vendor, VendorDTO vendorDTO);
}

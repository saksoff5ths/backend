package com.saksoffth.backend.mapper;

import com.saksoffth.backend.dto.PurchaseOrderDTO;
import com.saksoffth.backend.dto.PurchaseOrderItemDTO;
import com.saksoffth.backend.entity.PurchaseOrderItem;
import org.mapstruct.Mapper;

/**
 * Mapper for {@link PurchaseOrderItem} domain entity.
 */
@Mapper(componentModel = "spring")
public interface PurchaseOrderItemMapper {

    /**
     * Maps {@link PurchaseOrderDTO} from {@link PurchaseOrderItem}.
     *
     * @param purchaseOrderItem instance of {@link PurchaseOrderItem}
     * @return instance of {@link PurchaseOrderDTO}
     */
    PurchaseOrderItemDTO toDTO(PurchaseOrderItem purchaseOrderItem);

    /**
     * Maps {@link PurchaseOrderItem} from {@link PurchaseOrderItemDTO}.
     *
     * @param purchaseOrderItemDTO instance of {@link PurchaseOrderItemDTO}
     */
    PurchaseOrderItem fromDTO(PurchaseOrderItemDTO purchaseOrderItemDTO);
}

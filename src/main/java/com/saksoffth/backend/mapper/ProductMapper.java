package com.saksoffth.backend.mapper;

import com.saksoffth.backend.dto.CustomerProductDTO;
import com.saksoffth.backend.dto.ProductDTO;
import com.saksoffth.backend.entity.Product;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;

/**
 * Mapper for {@link Product} domain entity.
 */
@Mapper(componentModel = "spring")
public interface ProductMapper {

    /**
     * Maps {@link ProductDTO} from {@link Product}.
     *
     * @param product instance of {@link Product}
     * @return instance of {@link ProductDTO}
     */
    ProductDTO toDTO(Product product);

    /**
     * Maps {@link Product} from {@link ProductDTO}.
     *
     * @param productDTO instance of {@link ProductDTO}
     */
    @Mapping(target = "id", ignore = true)
    Product fromDTO(ProductDTO productDTO);

    /**
     * Maps {@link Product} from {@link ProductDTO}.
     *
     * @param product    instance of {@link Product}
     * @param productDTO instance of {@link ProductDTO}
     */
    @Mapping(target = "id", ignore = true)
    void fromDTO(@MappingTarget Product product, ProductDTO productDTO);

    CustomerProductDTO toCustomerDTO(Product product);
}

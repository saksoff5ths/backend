package com.saksoffth.backend.mapper;

import com.saksoffth.backend.dto.PaymentCardDTO;
import com.saksoffth.backend.entity.Address;
import com.saksoffth.backend.entity.PaymentCard;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;

/**
 * Mapper for {@link PaymentCard} domain entity.
 */
@Mapper(componentModel = "spring")
public interface PaymentCardMapper {

    /**
     * Maps {@link PaymentCardDTO} from {@link Address}.
     *
     * @param paymentCard instance of {@link PaymentCard}
     * @return instance of {@link PaymentCardDTO}
     */
    PaymentCardDTO toDTO(PaymentCard paymentCard);

    /**
     * Maps {@link PaymentCard} from {@link PaymentCardDTO}.
     *
     * @param paymentCardDTO instance of {@link PaymentCardDTO}
     */
    @Mapping(target = "id", ignore = true)
    PaymentCard fromDTO(PaymentCardDTO paymentCardDTO);

    /**
     * Maps {@link PaymentCard} from {@link PaymentCardDTO}.
     *
     * @param paymentCard    instance of {@link PaymentCard}
     * @param paymentCardDTO instance of {@link PaymentCardDTO}
     */
    @Mapping(target = "id", ignore = true)
    void fromDTO(@MappingTarget PaymentCard paymentCard, PaymentCardDTO paymentCardDTO);
}

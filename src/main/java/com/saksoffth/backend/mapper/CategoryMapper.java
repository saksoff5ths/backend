package com.saksoffth.backend.mapper;

import com.saksoffth.backend.dto.CategoryDTO;
import com.saksoffth.backend.dto.CustomerCategoryDTO;
import com.saksoffth.backend.dto.SelectableDTO;
import com.saksoffth.backend.entity.Category;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;

/**
 * Mapper for {@link Category} domain entity.
 */
@Mapper(componentModel = "spring")
public interface CategoryMapper {

    /**
     * Maps {@link CategoryDTO} from {@link Category}.
     *
     * @param category instance of {@link Category}
     * @return instance of {@link CategoryDTO}
     */
    CategoryDTO toDTO(Category category);

    /**
     * Maps {@link Category} from {@link CategoryDTO}.
     *
     * @param categoryDTO instance of {@link CategoryDTO}
     */
    @Mapping(target = "id", ignore = true)
    @Mapping(target = "parent", ignore = true)
    Category fromDTO(CategoryDTO categoryDTO);

    /**
     * Maps {@link Category} from {@link CategoryDTO}.
     *
     * @param category    instance of {@link Category}
     * @param categoryDTO instance of {@link CategoryDTO}
     */
    @Mapping(target = "id", ignore = true)
    @Mapping(target = "parent", ignore = true)
    void fromDTO(@MappingTarget Category category, CategoryDTO categoryDTO);

    /**
     * Maps {@link SelectableDTO} from {@link Category}.
     *
     * @param category instance of {@link Category}.
     * @return instance of {@link SelectableDTO}.
     */
    SelectableDTO toSelectable(Category category);


    /**
     * Maps {@link CustomerCategoryDTO} from {@link Category}.
     *
     * @param category instance of {@link Category}.
     * @return instance of {@link CustomerCategoryDTO}.
     */
    CustomerCategoryDTO toCustomerDTO(Category category);
}

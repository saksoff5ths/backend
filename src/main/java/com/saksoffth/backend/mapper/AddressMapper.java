package com.saksoffth.backend.mapper;

import com.saksoffth.backend.dto.AddressDTO;
import com.saksoffth.backend.entity.Address;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;

/**
 * Mapper for {@link Address} domain entity.
 */
@Mapper(componentModel = "spring")
public interface AddressMapper {

    /**
     * Maps {@link AddressDTO} from {@link Address}.
     *
     * @param address instance of {@link Address}
     * @return instance of {@link AddressDTO}
     */
    AddressDTO toDTO(Address address);

    /**
     * Maps {@link Address} from {@link AddressDTO}.
     *
     * @param addressDTO instance of {@link AddressDTO}
     */
    @Mapping(target = "id", ignore = true)
    Address fromDTO(AddressDTO addressDTO);

    /**
     * Maps {@link Address} from {@link AddressDTO}.
     *
     * @param address    instance of {@link Address}
     * @param addressDTO instance of {@link AddressDTO}
     */
    @Mapping(target = "id", ignore = true)
    void fromDTO(@MappingTarget Address address, AddressDTO addressDTO);
}

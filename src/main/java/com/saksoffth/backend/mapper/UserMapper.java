package com.saksoffth.backend.mapper;

import java.util.Arrays;
import javax.servlet.http.HttpServletRequest;

import com.saksoffth.backend.dto.LoginDTO;
import com.saksoffth.backend.dto.UserDTO;
import com.saksoffth.backend.dto.UserDetailsDTO;
import com.saksoffth.backend.dto.UserSessionDTO;
import com.saksoffth.backend.entity.User;
import com.saksoffth.backend.entity.UserRole;
import com.saksoffth.backend.entity.UserStatus;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.Named;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;

/**
 * Mapper for {@link User} domain entity.
 */
@Mapper(componentModel = "spring", imports = {Arrays.class})
public interface UserMapper {

    /**
     * Maps {@link UserDTO} from {@link User}.
     *
     * @param user instance of {@link User}
     * @return instance of {@link UserDTO}
     */
    UserDTO toDTO(User user);

    /**
     * Maps {@link User} from {@link UserDTO}.
     *
     * @param userDTO instance of {@link UserDTO}
     */
    @Mapping(target = "id", ignore = true)
    User fromDTO(UserDTO userDTO);

    /**
     * Maps {@link User} from {@link UserDTO}.
     *
     * @param user    instance of {@link User}
     * @param userDTO instance of {@link UserDTO}
     */
    @Mapping(target = "id", ignore = true)
    void fromDTO(@MappingTarget User user, UserDTO userDTO);

    /**
     * Maps {@link User} from {@link UserDTO}.
     *
     * @param user     instance of {@link User}
     * @param password encoded password.
     */
    void fromDTO(@MappingTarget User user, String password);

    /**
     * Maps {@link User} status field.
     *
     * @param user   instance of {@link User}
     * @param status instance of {@link UserStatus}
     */
    void fromDTO(@MappingTarget User user, UserStatus status);

    /**
     * Maps {@link User} from {@link UserDetailsDTO}.
     *
     * @param user instance of {@link User}
     * @return instance of {@link UserDetailsDTO}
     */
    @Mapping(target = "authorities", expression = "java(Arrays.asList(toAuthority(user)))")
    UserDetailsDTO toUserDetailsDTO(User user);

    @Mapping(target = "role", source = "user.role", qualifiedByName = "prefix")
    SimpleGrantedAuthority toAuthority(User user);

    @Named("prefix")
    static String prefix(UserRole role) {
        return "ROLE_" + role;
    }

    /**
     * Maps {@link UsernamePasswordAuthenticationToken} from {@link LoginDTO}.
     *
     * @param userDetails instance of {@link UserDetails}
     * @param request     instance of {@link HttpServletRequest}
     * @return instance of {@link Authentication}
     */
    default Authentication toAuthToken(UserDetails userDetails, HttpServletRequest request) {
        var authentication = new UsernamePasswordAuthenticationToken(userDetails, null, userDetails.getAuthorities());
        if (request != null) {
            authentication.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
        }
        return authentication;
    }

    /**
     * Maps {@link Authentication} from {@link LoginDTO}.
     *
     * @param dto instance of {@link LoginDTO}
     * @return instance of {@link Authentication}
     */
    default Authentication toAuthToken(LoginDTO dto) {
        return new UsernamePasswordAuthenticationToken(dto.getUsername(), dto.getPassword());
    }

    /**
     * Maps token to {@link UserSessionDTO}.
     *
     * @param token token.
     * @return instance of {@link UserSessionDTO}.
     */
    UserSessionDTO toUserSessionToken(String token);

}

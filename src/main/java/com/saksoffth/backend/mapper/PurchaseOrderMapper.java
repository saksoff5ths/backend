package com.saksoffth.backend.mapper;

import com.saksoffth.backend.dto.PurchaseOrderDTO;
import com.saksoffth.backend.entity.PurchaseOrder;
import org.mapstruct.Mapper;

/**
 * Mapper for {@link PurchaseOrder} domain entity.
 */
@Mapper(componentModel = "spring")
public interface PurchaseOrderMapper {

    /**
     * Maps {@link PurchaseOrderDTO} from {@link PurchaseOrder}.
     *
     * @param purchaseOrder instance of {@link PurchaseOrder}
     * @return instance of {@link PurchaseOrderDTO}
     */
    PurchaseOrderDTO toDTO(PurchaseOrder purchaseOrder);

    /**
     * Maps {@link PurchaseOrder} from {@link PurchaseOrderDTO}.
     *
     * @param purchaseOrderDTO instance of {@link PurchaseOrderDTO}
     */
    PurchaseOrder fromDTO(PurchaseOrderDTO purchaseOrderDTO);
}

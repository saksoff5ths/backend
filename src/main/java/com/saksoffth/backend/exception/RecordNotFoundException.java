package com.saksoffth.backend.exception;

public class RecordNotFoundException extends RuntimeException {

    public RecordNotFoundException(String message) {
        super(message);
    }

    public RecordNotFoundException(String entityName, Long id) {
        super(String.format("%s with %d not found!", entityName, id));
    }
}

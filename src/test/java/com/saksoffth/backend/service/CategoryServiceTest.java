package com.saksoffth.backend.service;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import com.saksoffth.backend.entity.Category;
import com.saksoffth.backend.exception.RecordNotFoundException;
import com.saksoffth.backend.repository.CategoryRepository;
import com.saksoffth.backend.service.impl.CategoryServiceImpl;
import com.saksoffth.backend.utils.PodamUtils;
import com.saksoffth.backend.utils.StrUtils;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;

/**
 * Test for service {@link CategoryService}.
 */
@ExtendWith(MockitoExtension.class)
class CategoryServiceTest {

    @Mock
    private CategoryRepository categoryRepository;

    @InjectMocks
    private CategoryServiceImpl categoryService;
    private final Long fakeId = 524L;

    @Test
    void testGetListEmpty() {
        var unPaged = Pageable.unpaged();
        given(categoryRepository.findAll(unPaged)).willReturn(Page.empty());
        var list = categoryService.getList(unPaged);

        verify(categoryRepository).findAll(unPaged);
        assertNotNull(list);
        assertEquals(0L, list.getTotalElements());
    }

    @Test
    void testGetList() {
        var pageable = Pageable.ofSize(3);
        var categories = List.of(generateCategory(), generateCategory(), generateCategory());
        var result = new PageImpl<>(categories, pageable, 3);
        given(categoryRepository.findAll(pageable)).willReturn(result);
        var list = categoryService.getList(pageable);

        verify(categoryRepository).findAll(pageable);
        assertNotNull(list);
        assertEquals(3L, list.getTotalElements());
        assertEquals(list.getContent(), categories);
    }

    @Test
    void testGetItem() {
        var expected = generateCategory();
        given(categoryRepository.findById(fakeId)).willReturn(Optional.of(expected));

        var actual = categoryService.getItem(fakeId);

        verify(categoryRepository).findById(fakeId);
        assertNotNull(actual);
        assertEquals(actual, expected);
    }

    @Test
    void testGetItemWithError() {
        given(categoryRepository.findById(fakeId)).willReturn(Optional.empty());
        assertThrows(RecordNotFoundException.class, () -> categoryService.getItem(fakeId));

        verify(categoryRepository).findById(fakeId);
    }

    @Test
    void testSaveItem() {
        Category category = generateCategory();
        categoryService.saveItem(category);
        verify(categoryRepository).save(category);
    }

    @Test
    void testDeleteItem() {
        categoryService.deleteItem(fakeId);
        verify(categoryRepository).deleteById(fakeId);
    }

    @Test
    void testSearch() {
        given(categoryRepository.findAllByNameLike(StrUtils.getSearchText("test"))).willReturn(Collections.emptyList());
        List<Category> test = categoryService.search("test");

        verify(categoryRepository).findAllByNameLike(StrUtils.getSearchText("test"));
        assertEquals(Collections.emptyList(), test);
    }

    private Category generateCategory() {
        return PodamUtils.manufacturePojo(Category.class);
    }
}

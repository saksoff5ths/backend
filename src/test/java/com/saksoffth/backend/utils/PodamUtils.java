package com.saksoffth.backend.utils;

import java.lang.reflect.Type;

import lombok.experimental.UtilityClass;
import uk.co.jemos.podam.api.PodamFactory;
import uk.co.jemos.podam.api.PodamFactoryImpl;

/**
 * Utility operations for generating data via Podam library.
 */
@UtilityClass
public class PodamUtils {
    private static final PodamFactory PODAM_FACTORY;

    static {
        PODAM_FACTORY = new PodamFactoryImpl();
    }

    /**
     * Creates and generates data for object of T class.
     *
     * @param type            - type.
     * @param <T>             - Class.
     * @param genericTypeArgs - the generic Type arguments for a generic class instance.
     * @return object with generated data.
     */
    public static <T> T manufacturePojo(Class<T> type, Type... genericTypeArgs) {
        return PODAM_FACTORY.manufacturePojo(type, genericTypeArgs);
    }
}
